/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pozopetrolero;

import java.util.ArrayList;
import java.util.Random;
/**
 *
 * @author cesar
 */
public class Pozo {
        
    private ArrayList<Integer> celdasBase = new ArrayList<>();
    
    private Random rnd = new Random();
    /*
     llenar la matriz pozo con celdas que representen piedras(false) y 
     petroleo (true)
     */

    public void llenarPozo(Celda[][] pozo, float dP) {
        for (int a = 0; a < pozo.length; a++) {
            for (int b = 0; b < pozo.length; b++) {
                pozo[a][b] = new Celda();
//                pozo[a][b].setEsPetroleo(rnd.nextBoolean());
                double r = rnd.nextDouble();
                boolean resultado;
                if(r > dP){
                    resultado=true;
                }else{
                    resultado=false;
                }
                    
                pozo[a][b].setEsPetroleo(resultado);
            }
        }
    }

    //verificar que hay celdas con petroleo en la base del pozo
    private void celdasBase(Celda[][] pozo) {
        celdasBase.clear();
        boolean repetido = false;
        for (int a = 0; a < pozo.length; a++) {
            if (pozo[pozo.length - 1][a].getEsPetroleo()) {
                if (!repetido) {
                    celdasBase.add(a);
                }
                repetido = true;
            } else {
                repetido = false;
            }
        }
    }
    
    public int hayCamino(Celda[][] pozo) {
        celdasBase(pozo);
        if (celdasBase.isEmpty()) {
            return 0;
        }

        int fila, columna;

        while (!celdasBase.isEmpty()) {
            fila = pozo.length - 1;
            columna = celdasBase.remove(0);
            if (resolver(pozo, fila, columna)) {
                return 1;
            }
        }

        return 0;
    }

    private boolean resolver(Celda[][] pozo, int fila, int columna) {
        pozo[fila][columna].setFueRecorrido(true);
        if (fila == 0) {
            //hay solucion
            return true;
        }
        boolean temporal;
        
        //arriba
        if (fila - 1 >= 0 && pozo[fila - 1][columna].getEsPetroleo() && !pozo[fila - 1][columna].getFueRecorrido()) {
            temporal = resolver(pozo, fila - 1, columna);
            if (temporal) {
                return true;
            }
        }
        //abajo
        if (fila + 1 < pozo.length && pozo[fila + 1][columna].getEsPetroleo() && !pozo[fila + 1][columna].getFueRecorrido()) {
            temporal = resolver(pozo, fila + 1, columna);
            if (temporal) {
                return true;
            }
        }
        //izquierda
        if (columna - 1 >= 0 && pozo[fila][columna - 1].getEsPetroleo() && !pozo[fila][columna - 1].getFueRecorrido()) {
            temporal = resolver(pozo, fila, columna - 1);
            if (temporal) {
                return true;
            }
        }
        //derecha
        if (columna + 1 < pozo.length && pozo[fila][columna + 1].getEsPetroleo() && !pozo[fila][columna + 1].getFueRecorrido()) {
            temporal = resolver(pozo, fila, columna + 1);
            if (temporal) {
                return true;
            }
        }
        
        //no hay solucion
//        pozo[fila][columna].setFueRecorrido(false);
        return false;
    }
    
//    public void limpiarHistorialRecorrido(Celda[][] pozo){
//        for (int a = 0; a < pozo.length; a++) {
//            for (int b = 0; b < pozo.length; b++) {
//                pozo[a][b].setFueRecorrido(false);
//            }
//        }
//    }
    
    /*
    imprime la matriz que representa el pozo.
    los espacios en blanco son el petroleo y las "X" son las piedras
    */
    public void imprimir(Celda[][] pozo){
        for (int a = 0; a < pozo.length; a++) {
            System.out.print("| ");
            for (int b = 0; b < pozo.length; b++) {
                System.out.print((pozo[a][b].getEsPetroleo()?" ":"X") + " ");
            }
            System.out.println("|");
        }
    }
    
    /*
    imprime el recorrido que se hizo en el pozo con puntos
    */
    public void imprimirRecorrido(Celda[][] pozo){
        for (int a = 0; a < pozo.length; a++) {
            System.out.print("| ");
            for (int b = 0; b < pozo.length; b++) {
                System.out.print((pozo[a][b].getEsPetroleo()?(pozo[a][b].getFueRecorrido()?".":" "):"X") + " ");
            }
            System.out.println("|");
        }
    }
}
