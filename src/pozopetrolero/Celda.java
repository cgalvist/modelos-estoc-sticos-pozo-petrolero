/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pozopetrolero;

/**
 *
 * @author cesar
 */
public class Celda {
    private boolean esPetroleo;
    private boolean fueRecorrido = false;

    public boolean getEsPetroleo() {
        return esPetroleo;
    }

    public void setEsPetroleo(boolean esPetroleo) {
        this.esPetroleo = esPetroleo;
    }

    public boolean getFueRecorrido() {
        return fueRecorrido;
    }

    public void setFueRecorrido(boolean fueRecorrido) {
        this.fueRecorrido = fueRecorrido;
    }
}
