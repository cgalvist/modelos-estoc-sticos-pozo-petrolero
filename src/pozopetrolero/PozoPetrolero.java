/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pozopetrolero;

import java.util.ArrayList;

/**
 *
 * @author cesar
 */
public class PozoPetrolero {

    
//    private ArrayList<String> celdasPendientes = new ArrayList<>();
    private static Celda[][] matrizPozo;

    public static void main(String[] args) {
        int n = 100;
        int iteraciones = 100000;
        int numeroIteraciones = iteraciones;
        
        float probabilidad = 0.0f;
        float dPInicial = 0.01f;
        
        
        System.out.println("Tama%o de la matriz (n): " + n);
        System.out.println("Numero de iteraciones: " + iteraciones + "\n");
//        System.out.println("probabilidad solido: " + probabilidad);
        ArrayList <Double> puntos = new ArrayList<>();
        //puntos.add(1.0);
        
        int numeroDeExitos = 0;
        
        matrizPozo = new Celda[n][n];
        double tP;
        
        
        while(probabilidad <= 1){
//            System.out.println("dP: " + dP);
            
            System.out.println(probabilidad);
            
            numeroDeExitos = 0;
            for(int b = 0; b < numeroIteraciones; b++){

                Pozo pozo = new Pozo();
                pozo.llenarPozo(matrizPozo, probabilidad);

//                System.out.println("");

                int hayCamino = pozo.hayCamino(matrizPozo);

//                System.out.println("iteracion " + b);
                
//                System.out.println("fue exitoso? " + hayCamino);

                if(hayCamino == 1){
                    numeroDeExitos++;
                }
//                    System.out.println("Este fue el camino recorrido: \n");
//                    pozo.imprimirRecorrido(matrizPozo);
//                System.out.println("\n --------------------------- \n");
//                }
            }
            
//            System.out.println("numero de exitos: " + numeroDeExitos);
            
            tP = (double) numeroDeExitos / (double) numeroIteraciones;
            
//            System.out.println(numeroDeExitos + " wwww " + numeroIteraciones);
            
//            System.out.println("theta P: " + tP);
            
            puntos.add(tP);
            
            probabilidad =  probabilidad + dPInicial;
            
//            iteraciones = numeroIteraciones;
        }

        System.out.println("lista theta P: " + puntos);
    }
    
}
